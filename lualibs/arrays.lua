local M = {}

--- Returns the longest string in the array
--- If the array is empty then returns an empty string
---@param a string[]
---@return string
M.longest_string = function(a)
	local r = ""
	for _, s in ipairs(a) do
		r = s:len() > r:len() and s or r
	end
	return r
end

---@generic T
---@param array T[]
---@param ele T
---@return boolean
M.contains = function(array, ele)
	for _, a in pairs(array) do
		if a == ele then
			return true
		end
	end
	return false
end

---@generic T
---@generic K
---@param array T[]
---@param condition fun(v: T, i: integer): boolean
---@return T[]
M.filter = function(array, condition)
	local res = {}
	for i, a in pairs(array) do
		if condition(a, i) then
			res[#res + 1] = a
		end
	end
	return res
end

---@generic T
---@generic K
---@param array T[]
---@param condition fun(v: T, i: integer): boolean
---@return T | nil, integer
M.find = function(array, condition)
	local res = {}
	for i, a in pairs(array) do
		if condition(a, i) then
			return res, i
		end
	end
	return nil, 0
end

---@generic T
---@generic K
---@param array T[]
---@param handler fun(v: T, i: integer): K
---@return K[]
M.map = function(array, handler)
	local res = {}
	for i, a in pairs(array) do
		res[#res + 1] = handler(a, i)
	end
	return res
end

return M
