local M = {}

local os_posix = require("lualibs.os_extended.posix_run")
--- @param cmd string
--- @param args? string[]
--- @return string[] out
--- @return string[] err
--- @return boolean ok
--- @return integer code
M.run2 = function(cmd, args)
	local err = {}
	local out = {}

	local ok, code = os_posix.run(cmd, args, {
		on_stderr_line = function(e) err[#err + 1] = e end,
		on_stdout_line = function(o) out[#out + 1] = o end,
	})
	return out, err, ok, code
end

---@param cmd string a command to execute
---@param args? table<integer, string> the list of arguments to the command
local function valid_command(cmd, args)
	if string.match(cmd, [["]]) then
		return false
	end

	if string.match(cmd, "%s+") then
		return false
	end

	if args then
		for _, arg in ipairs(args) do
			if string.match(arg, [["]]) then
				return false
			end
		end
	end

	return true
end

--- Given a cmd with its parameters, it returns the formatted command for `os.execute`
---@return string
local function prepare_command(cmd, args)
	local sargs = nil
	if args then
		sargs = {}
		for _, arg in ipairs(args) do
			sargs[#sargs + 1] = '"' .. arg .. '"'
		end
	end

	local str_cmd = cmd
	if sargs then
		str_cmd = str_cmd .. " " .. table.concat(sargs, " ")
	end
	return str_cmd
end

---@class Run1Opts

---@type Run1Opts
local DEFAULT_OPTS = {
	parallel = false,
	nostderr = true,
}

---execute a command and returns the corresponding output and error codes
---@param cmd string a command to execute
---@param args? string[] the list of arguments to the command
---@param opts? Run1Opts options for the function
---@return string[] output output of the command divided by lines
---@return boolean? ok if the command was run successfully
---@return integer? code exit code of the command
M.run = function(cmd, args, opts)
	opts = opts or DEFAULT_OPTS

	if not valid_command(cmd, args) then
		return {}, false, 100
	end

	local scmd = prepare_command(cmd, args)

	if opts.parallel then
		scmd = scmd .. " &"
	end

	if opts.nostderr then
		scmd = "(" .. scmd .. ") 2>/dev/null"
	end

	local handle, _ = io.popen(scmd)
	if handle == nil then
		return {}, false, -1
	end

	local output = {}
	for line in handle:lines("*l") do
		output[#output + 1] = line
	end
	local ok, _, code = handle:close()
	return output, ok, code
end

---execute a series of commands in a pipe
---@param progs {[1]: string, [2]: string[]}[] a list of commands with their respective arguments to execute in order as a pipe
---@param opts? RunOpts options for the function
---@return string[] output output of the command divided by lines
---@return boolean? ok if the command was run successfully
---@return integer? code exit code of the command
M.pipe = function(progs, opts)
	opts = opts or DEFAULT_OPTS

	---@type string[]
	local cmds = {}
	for _, prog in pairs(progs) do
		local cmd = prog[1]
		local args = prog[2]

		if not valid_command(cmd, args) then
			return {}, false, 100
		end

		cmds[#cmds + 1] = prepare_command(cmd, args)
	end
	local scmds = table.concat(cmds, " | ")

	if opts.parallel then
		scmds = scmds .. " &"
	end

	local handle, _ = io.popen(scmds)
	if handle == nil then
		return {}, false, -1
	end

	local output = {}
	for line in handle:lines("*l") do
		output[#output + 1] = line
	end
	local ok, _, code = handle:close()
	return output, ok, code
end

M.pwd = function()
	local output, _, _ = M.run("pwd")
	return output[1]
end

M.is_running = function(prog)
	local _, ok, _ = M.run("pgrep", prog)
	return ok
end

M.cmd_exists = function(prog)
	local _, ok, _ = M.run("which", { prog })
	return ok
end

M.mkdir = function(path, parent)
	local opts = {}
	if parent then
		opts[#opts + 1] = "-p"
	end
	opts[#opts + 1] = path

	return M.run("mkdir", opts)
end

M.notify = function(title, message, icon, urgency)
	local opts = {}
	if icon then
		opts[#opts + 1] = "-i"
		opts[#opts + 1] = icon
	end

	if urgency then
		opts[#opts + 1] = "-u"
		opts[#opts + 1] = urgency
	end

	opts[#opts + 1] = title

	if message then
		opts[#opts + 1] = message
	end

	M.run("notify-send", opts)
end

return M
