local M = {}
local unistd = require("posix.unistd")

---@alias FD integer

---@class Pipe
---@field read FD
---@field write FD
---@field buffer string[]
---@field pcount integer
---@field eof boolean
---@field unread? string

--- @return Pipe
--- @return boolean ok
--- @return integer status
local function pipe()
	local r, w = unistd.pipe()
	if r == nil then
		return {}, false, 1
	end
	return {
		write = w,
		read = r,
		buffer = {},
		pcount = 0,
		eof = false,
	},
		true,
		0
end

--- @param s string
--- @return string[]
local function get_lines(s)
	local lines = {}
	for l in string.gmatch(s, "[^\n]*") do
		lines[#lines + 1] = l
	end
	return lines
end

---@param p Pipe
---@return string line
---@return boolean ok
---@return string error
local function read_line(p)
	local CHUNK = 16
	if p.pcount > #p.buffer and p.eof then
		return "", false, ""
	end
	if p.pcount == #p.buffer then
		local line_buffer = ""
		if #p.buffer > 0 then
			line_buffer = p.buffer[#p.buffer] or ""
		end

		local buffer_loading = true
		while buffer_loading and not p.eof do
			local str, rerr, rcode = unistd.read(p.read, CHUNK)
			if rcode ~= nil then
				p.eof = true
				return "", false, rerr
			end

			if str == nil or str == "" then
				p.eof = true
				buffer_loading = false
				if #p.buffer == 0 then
					return "", false, rerr
				end
			else
				line_buffer = string.format("%s%s", line_buffer, str)
				if string.find(str, "[\n]") then
					buffer_loading = false
				end
			end
		end

		local lines = get_lines(line_buffer)
		p.buffer = lines
		p.pcount = 1
	end

	local line = p.buffer[p.pcount]
	p.pcount = p.pcount + 1
	return line, true, ""
end

---@class RunOpts
---@field async? boolean
---@field on_stdout_line fun(s: string)
---@field on_stderr_line fun(s: string)

---execute a command and returns the corresponding output and error codes
---@param cmd string a command to execute
---@param args? string[] the list of arguments to the command
---@param opts? RunOpts options for the function
---@return boolean ok
---@return integer code
M.run = function(cmd, args, opts)
	args = args or {}
	opts = opts or {}
	opts.on_stdout_line = opts and opts.on_stdout_line or function() end
	opts.on_stderr_line = opts and opts.on_stderr_line or function() end

	local stdout, pook, pocode = pipe()
	if not pook then
		print("Can't create pipe stdout")
		return false, pocode
	end

	local stderr, peok, pecode = pipe()
	if not peok then
		print("Can't create pipe stderr")
		return false, pecode
	end

	local pid = unistd.fork()
	if pid == nil then
		return false, -1
	end

	if pid == 0 then
		unistd.close(stdout.read)
		unistd.close(stderr.read)

		unistd.dup2(stdout.write, unistd.STDOUT_FILENO)
		unistd.dup2(stderr.write, unistd.STDERR_FILENO)

		local _, _, ecode = unistd.execp(cmd, args)

		unistd.close(stdout.write)
		unistd.close(stderr.write)
		os.exit(ecode)
	else
		unistd.close(stdout.write)
		unistd.close(stderr.write)

		local stdoutRunning = true
		local stderrRunning = true
		while stdoutRunning or stderrRunning do
			if stdoutRunning then
				local oline, ook, _ = read_line(stdout)
				stdoutRunning = ook
				if stdoutRunning then
					opts.on_stdout_line(oline)
				end
			end

			if stderrRunning then
				local eline, eok, _ = read_line(stderr)
				stderrRunning = eok
				if stderrRunning then
					opts.on_stderr_line(eline)
				end
			end
		end

		local _, _, status = require("posix.sys.wait").wait(pid)
		return status == 0, status
	end
end

return M
