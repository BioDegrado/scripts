local M = {}
local ose = require("lualibs.os_extended")

---@alias LogLevel integer
M.LOG_LEVEL = {
	NONE = 0,
	DEBUG = 1,
	INFO = 2,
	WARN = 4,
	ERROR = 8,
}

local LOG_FORMAT = "%s[%s]:%s:: %s"
local DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
local LOG_DIR = os.getenv("HOME") .. "/.cache/logs/scripts/"
local LOG_LEVEL_ALL = M.LOG_LEVEL.DEBUG
	| M.LOG_LEVEL.INFO
	| M.LOG_LEVEL.WARN
	| M.LOG_LEVEL.ERROR

---@param logfun fun(severity: LogLevel, msg: string | string[])
---@param loglevel LogLevel
local function makelogger(logfun, loglevel)
	local logger = {}
	local emptylog = function() end
	for level, lcode in pairs(M.LOG_LEVEL) do
		local logactive = lcode & loglevel ~= 0
		local llevel = string.lower(level)
		if logactive then
			logger[llevel] = function(msg) logfun(lcode, msg) end
			logger[llevel .. "f"] = function(f, ...)
				logfun(lcode, string.format(f, ...))
			end
		else
			logger[llevel] = emptylog
			logger[llevel .. "f"] = emptylog
		end
	end
	return logger
end

local function getfile(name)
	local today = string.format("%s.log", os.date("%Y-%m-%d"))
	local logdir = LOG_DIR .. name
	local filepath = logdir .. "/" .. today
	ose.mkdir(logdir, true)
	return io.open(filepath, "a+")
end

M.getlogger = function(name, loglevel, dateformat)
	dateformat = dateformat or DATE_FORMAT
	if loglevel == nil then
		loglevel = LOG_LEVEL_ALL
	end

	local format = LOG_FORMAT
	local file = getfile(name)

	local SEVERITY = {
		[M.LOG_LEVEL.DEBUG] = "DGB",
		[M.LOG_LEVEL.INFO] = "INF",
		[M.LOG_LEVEL.WARN] = "WRN",
		[M.LOG_LEVEL.ERROR] = "ERR",
	}

	---@param severity LogLevel
	---@param msg string | string[]
	local function log(severity, msg)
		local time = os.date(dateformat)
		local out = ""
		if type(msg) == "string" then
			out = string.format(format, name, SEVERITY[severity], time, msg)
		else
			out = table.concat(msg, "\n")
		end

		print(out)
		if file then
			file:write(out .. "\n")
		end
	end

	return makelogger(log, loglevel)
end

return M
