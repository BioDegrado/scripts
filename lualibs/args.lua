local M = {}

local arrays = require("lualibs.arrays")
local strings = require("lualibs.string")

---@alias narrowType "number" | "string" | "boolean"

---@class ArgsOpts
---@field help_if_no_args boolean?
---@field args ArgConf[]

---@class ArgConf
---@field name ArgName
---@field description string
---@field receives? narrowType
---@field optional? boolean
---@field onReceive fun(arg?: number | string | boolean)

---@class ArgName
---@field short? string
---@field full? string

---@param arg string
---@param conf ArgConf
---@return boolean
local function matches_arg(arg, conf)
	local name = conf.name
	if name.short and arg:match(name.short) then
		return true
	end
	if name.full and arg:match(name.full) then
		return true
	end
	return false
end

---@param arg string
---@param configs ArgConf[]
---@return ArgConf | nil, integer
local function get_config(arg, configs)
	for i, conf in ipairs(configs) do
		if matches_arg(arg, conf) then
			return conf, i
		end
	end
	return nil, 0
end

---@param opts ArgsOpts
---@return boolean
local function validate_arg_opts(opts) return true end

---@param configs ArgConf[]
---@return boolean
local function has_help(configs)
	for _, conf in ipairs(configs) do
		local has_short = conf.name.short == "-h"
		local has_full = conf.name.full == "--help"
		if has_short or has_full then
			return true
		end
	end
	return false
end

---@param configs ArgConf[]
local function print_help(configs)
	local full_names = {}
	for _, conf in ipairs(configs) do
		full_names[#full_names + 1] = conf.name.full
	end
	local fname_maxl = arrays.longest_string(full_names):len()

	local descs = {}
	for _, conf in ipairs(configs) do
		descs[#descs + 1] = strings.split_into_lines(conf.description, 50)
	end

	local req = "[required]"
	local main_linef = ("%%3s %%-%ds %%-%ds : %%s"):format(
		fname_maxl,
		req:len()
	)

	local longest_opt = req:len()
	local padding = string.rep(" ", 3 + fname_maxl + longest_opt + 4)
	local supp_linef = padding .. "%s"

	for i, conf in pairs(configs) do
		local short = conf.name.short or ""
		local full = conf.name.full or ""
		local required = not conf.optional and req or ""
		local lines = descs[i]
		print(main_linef:format(short, full, required, lines[1]))
		if #lines > 1 then
			for j = 2, #lines do
				print(supp_linef:format(lines[j]))
			end
		end
	end
end

---@param configs ArgConf[]
---@return ArgConf
local function get_help_arg(configs)
	return {
		name = { short = "-h", full = "--help" },
		description = "Shows help message.",
		optional = true,
		onReceive = function() print_help(configs) end,
	}
end

---@class ArgsIterResult
---@field conf ArgConf
---@field arg string
---@field param? string

---@param opts ArgsOpts
---@param args string[]
---@param handler fun(res: ArgsIterResult, idx: integer)
local function args_iterator(opts, args, handler)
	for i = 1, #args do
		local arg = args[i]
		local config, idx = get_config(arg, opts.args)
		if config == nil then
			return
		end

		local param = nil
		if config.receives then
			if i + 1 <= #args then
				return
			end

			param = args[i + 1]
			i = i + 1
		end
		handler({
			conf = config,
			arg = arg,
			param = param,
		}, idx)
	end
end

---@param opts ArgsOpts
---@param args string[]
---@return boolean, ArgConf[]
local function has_required_args(opts, args)
	-- visited
	local vst = {}
	args_iterator(opts, args, function(res, i)
		if not res.conf.optional then
			vst[#vst + 1] = i
		end
	end)

	local a = opts.args
	a = arrays.filter(a, function(_, i) return not arrays.contains(vst, i) end)
	a = arrays.filter(a, function(v) return not v.optional end)
	if #a > 0 then
		return false, a
	end
	return true, {}
end

---@param opts ArgsOpts
---@return fun(args: string[]) | nil
M.get = function(opts)
	if opts.help_if_no_args == nil then
		opts.help_if_no_args = false
	end

	if not validate_arg_opts(opts) then
		return nil
	end

	if not has_help(opts.args) then
		opts.args[#opts.args + 1] = get_help_arg(opts.args)
	end

	return function(args)
		if #opts.args == 0 then
			print(">.>")
			return
		end

		local passed_help = arrays.find(
			args,
			function(v) return v == "-h" or v == "--help" end
		)
		local forced_no_help = #args == 0 and opts.help_if_no_args
		if forced_no_help or passed_help then
			local help = get_config("-h", opts.args)
				or get_config("--help", opts.args)
			if not help then
				print("LOL no help")
				return
			end
			help.onReceive()
			return
		end

		local required_args, missing = has_required_args(opts, args)
		if not required_args then
			print("lol did you forget something??")
			for _, conf in pairs(missing) do
				print(("\t%s, %s"):format(conf.name.short, conf.name.full))
			end
			return
		end

		args_iterator(opts, args, function(res)
			if res.param ~= nil then
				res.conf.onReceive(res.param)
			else
				res.conf.onReceive()
			end
		end)
	end
end

return M
