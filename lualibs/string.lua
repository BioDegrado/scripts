local M = {}

---@param text string
---@param max integer
---@return string[]
M.split_into_lines = function(text, max)
	if max < 0 then
		return {}
	end
	local lines = { "" }

	text:gsub("%w+", function(word)
		local line = lines[#lines]
		if line:len() + word:len() > max then
			lines[#lines + 1] = word
		else
			lines[#lines] = line .. word .. " "
		end
	end)

	return lines
end
return M
