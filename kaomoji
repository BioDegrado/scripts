#!/bin/bash

KAOMOJI_DB=$XDG_CONFIG_HOME/kaomoji/kaomoji.db
SEP=";:;:;"

remove_start_tags ()
{
	local regex_tag='<[a-zA-Z"= \/_!-]*?>'
	local regex_tags="^$regex_tag$regex_tag"
	echo "$1" | perl -pe "s/$regex_tags//g"
}

remove_ending_tags ()
{
	local regex_tag='<\/[a-zA-Z]+?>'
	local regex_tags="($regex_tag){1,2}\$"
	echo "$1" | perl -pe "s/$regex_tags//g"
}

force_empty_file ()
{
	if [ -d "$1" ]; then
		echo "Error: impossible to create the file, a directory already has the name!"
		exit 1
	fi
	if [ -f "$1" ]; then
		rm "$1"
	fi
	touch "$1"
}

unescape_less_greater_html ()
{
	local less_than='&lt;'
	local greater_than='&gt;'
	echo "$1" | sed "s/$less_than/</g;s/$greater_than/>/g"
}

install_kaomoji ()
{
	# Create an empty database and the relative directories
	mkdir --parents "$(dirname "$KAOMOJI_DB")"
	touch "$KAOMOJI_DB"

	local url="https://pc.net/emoticons/kaomoji"

	local website_content=$(curl "$url")

	local regex_table_content='\(^<tr>.*\)\|\(^<td class="meaning".*\)'
	local buffer=$(echo "$website_content" | grep --only-matching --regexp "$regex_table_content")
	local buffer=$(remove_start_tags "$buffer")
	local buffer=$(remove_ending_tags "$buffer")
	local buffer=$(unescape_less_greater_html "$buffer")

	local i=1
	local kaomoji=""
	force_empty_file "kaomoji.db"
	while read -r line; do
		if (("$i" % 2 != 0)); then
			kaomoji="$line"
		else
			echo "$kaomoji $SEP $line" >> "$KAOMOJI_DB"
		fi
		i=$((i+1))
	done <<< "$buffer"
}

select_kaomoji ()
{
	# curr_wid=$(xdo id)
	local select=$(rofi -i -dmenu -theme kaomoji <"$KAOMOJI_DB"  | tr -d \\n)

	# No selections
	if [ "$select" = "" ]; then
		notify-send "Kaomoji!" "Aborted!"
		exit 1
	fi

	echo -n "$select" | sed "s/$SEP.*$//"
}

if [ ! -e "$KAOMOJI_DB" ]; then
	notify-send 'Kaomojis not found! (´∩｀。)' 'Installing kaomojis... ᕕ( ᐛ )ᕗ'
	if ! install_kaomoji; then
		notify-send "Error: Aborting installation" "Cannot Install kaomojis (>.<)!"
		exit 1
	fi
	notify-send 'Kaomojis installed! (´∀`)' "YAY!"
fi

kaomoji=$(select_kaomoji)

wl-copy "$kaomoji"
